import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {TodoItem} from './todo-item';
import {TodoListService} from '../../service/todo-list.service';

@Component({
  selector: 'app-todo-list-item',
  templateUrl: './todo-list-item.component.html',
  styleUrls: ['./todo-list-item.component.scss']
})
export class TodoListItemComponent implements OnInit {

  @Input() todo: TodoItem
  @Output() deleteIcon = new EventEmitter<TodoItem>();
  //public todo: TodoItem;

  constructor(private todoService: TodoListService) {
  }

  ngOnInit() {
  }

  toggleDone(done) {
    this.todo.done !== done;
    this.todoService.setDoneTodo(this.todo)
    .catch(error=>this.todoService.showSnack(error.status + " " + error.message));
  }

  deleteTodo() {
    this.deleteIcon.emit(this.todo);

  }

}
